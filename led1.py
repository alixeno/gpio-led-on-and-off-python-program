import RPi.GPIO as gpio
import time

gpio.setmode(gpio.BCM)
gpio.setup(26,gpio.OUT)

def ledoff():
	gpio.output(26, gpio.HIGH)
	time.sleep(0.100)
	return;
ledoff();

gpio.cleanup()
print("GPIO ran fine")

